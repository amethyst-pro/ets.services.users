using System;
using Amethyst.Domain.ValueObjects;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Entities
{
    public readonly struct Card : IEquatable<Card>
    {
        public Card(CardId id, Money balance, CardState state)
        {
            Id = id;
            Balance = balance;
            State = state;
        }

        public CardId Id { get; }
        
        public Money Balance { get; }
        
        public CardState State { get; }

        public Card ChangeState(CardState state)
            => new Card(Id, Balance, state);

        public bool Equals(Card other)
            => Id.Equals(other.Id);

        public override bool Equals(object obj)
            => obj is Card other && Equals(other);

        public override int GetHashCode()
            => Id.GetHashCode();
    }
}