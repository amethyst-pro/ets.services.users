using System;
using System.Collections.Generic;
using Amethyst.Domain;
using Amethyst.Domain.ValueObjects;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Entities;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Domain.ValueObjects;

namespace Ets.Services.Users.Domain.Aggregates
{
    public sealed class User : AggregateBase<UserId>, IUser
    {
        private Username _username;
        private UserRole _role;
        private UserState _state;
        private bool _isRemoved;
        private Profile _profile;
        private bool _isAcceptAgreement;
        private readonly IChangesService _changesService;
        
        private readonly Dictionary<CardId, Card> _cards = new Dictionary<CardId, Card>();
        
        public User(
            UserId id,
            Username username,
            Email email,
            UserRole role, 
            IChangesService changesService) 
            : base(id)
        {
            _changesService = changesService;
            
            ApplyEvent(new UserCreated(id, username, email, role));
        }

        public User(
            UserId id, 
            long version, 
            IReadOnlyCollection<IDomainEvent> events, 
            IChangesService changesService) 
            : base(id, version, events)
        {
            _changesService = changesService;
        }

        public bool IsRemoved => _isRemoved;

        bool IUser.HasChanges => HasChanges();

        public long? CurrentVersion => Version.OrDefault();

        public UserSnapshot GetSnapshot()
            => new UserSnapshot(
                Id,
                _username, 
                _role, 
                _state, 
                _profile, 
                _cards.Values,
                _isAcceptAgreement, 
                _isRemoved);

        public UserChanged GetChanges(DateTimeOffset timestamp)
        {
            var snapshot = GetSnapshot();
            return _changesService.GetChanges(snapshot, timestamp);
        }

        public void ChangeUsername(Username username)
        {
            if (username.Equals(_username))
                return;
            
            ApplyEvent(new UsernameChanged(Id, username));
        }

        public void Activate()
        {
            if (_state == UserState.Active)
                return;
            
            ApplyEvent(new UserActivated(Id));
        }
        
        public void Block()
        {
            if (_state == UserState.Blocked)
                return;
            
            ApplyEvent(new UserBlocked(Id));
        }

        public void ChangeRole(UserRole role)
        {
            if (_role == role)
                return;
            
            ApplyEvent(new RoleChanged(Id, role));
        }
        
        public void ChangeName(Name name)
        {
            if (_profile.Name.Equals(name))
                return;
            
            ApplyEvent(new NameChanged(Id, name));
        }
        
        public void ChangeEmail(Email email)
        {
            if (_profile.Email.Equals(email))
                return;
            
            ApplyEvent(new EmailChanged(Id, email));
        }

        public void ChangePhone(PhoneNumber phone)
        {
            if (_profile.Phone.Equals(phone))
                return;
            
            ApplyEvent(new PhoneChanged(Id, phone));
        }

        public void ChangePhoto(Photo photo)
        {
            if (_profile.Photo.Equals(photo))
                return;
  
            ApplyEvent(new PhotoChanged(Id, photo));
        }

        public void AddOrUpdateCard(CardId cardId, Money balance)
        {
            if (_cards.TryGetValue(cardId, out var storedCard))
            {
                if (storedCard.Id.Equals(cardId) && storedCard.Balance.Equals(balance))
                    return;
                
                ApplyEvent(new CardUpdated(Id, cardId, balance, storedCard.State));
            }
            
            ApplyEvent(new CardUpdated(Id, cardId, balance, CardState.Active));
        }

        public void RemoveCard(CardId cardId)
        {
            if (!_cards.ContainsKey(cardId))
                return;
            
            ApplyEvent(new CardRemoved(Id, cardId));
        }
        
        public void BlockCard(CardId cardId)
        {
            if (!_cards.ContainsKey(cardId))
                return;
            
            ApplyEvent(new CardBlocked(Id, cardId));
        }
        
        public void ActivateCard(CardId cardId)
        {
            if (!_cards.ContainsKey(cardId))
                return;
            
            ApplyEvent(new CardActivated(Id, cardId));
        }
        
        public void Remove()
        {
            if (IsRemoved)
                return;
            
            ApplyEvent(new UserRemoved(Id));
        }

        protected override void OnApplyEvent(IDomainEvent @event)
            => When((dynamic) @event);

        private void When(UserCreated @event)
        {
            _username = @event.Username;
            _role = @event.Role;
            _state = @event.State;
            _isAcceptAgreement = @event.isAcceptAgreement;
            _profile = new Profile(
                default, 
                @event.Email, 
                default, 
                default);
        }

        private void When(UsernameChanged @event)
        {
            _username = @event.Username;
        }

        private void When(UserActivated @event)
        {
            _state = UserState.Active;
        }
        
        private void When(UserBlocked @event)
        {
            _state = UserState.Blocked;
        }
        
        private void When(RoleChanged @event)
        {
            _role = @event.Role;
        }

        private void When(NameChanged @event)
        {
            _profile = _profile.ChangeName(@event.Name);
        }
        
        private void When(EmailChanged @event)
        {
            _profile = _profile.ChangeEmail(@event.Email);
        }
        
        private void When(PhoneChanged @event)
        {
            _profile = _profile.ChangePhone(@event.Phone);
        }
        
        private void When(PhotoChanged @event)
        {
            _profile = _profile.ChangePhoto(@event.Photo);
        }
        
        private void When(CardUpdated @event)
        {
            _cards[@event.CardId] = new Card(@event.CardId, @event.Balance, @event.State);
        }
        
        private void When(CardRemoved @event)
        {
            _cards.Remove(@event.CardId);
        }
        
        private void When(CardBlocked @event)
        {
            _cards[@event.CardId] = _cards[@event.CardId].ChangeState(CardState.Blocked);
        }
        
        private void When(CardActivated @event)
        {
            _cards[@event.CardId] = _cards[@event.CardId].ChangeState(CardState.Active);
        }

        private void When(UserRemoved @event)
        {
            _state = UserState.Blocked;
            _isRemoved = true;
        }
    }
}