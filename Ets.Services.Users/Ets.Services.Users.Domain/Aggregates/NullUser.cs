using System;
using Amethyst.Domain.ValueObjects;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Domain.ValueObjects;

namespace Ets.Services.Users.Domain.Aggregates
{
    public sealed class NullUser : IUser
    {
        public long? CurrentVersion => default;
        
        public bool IsRemoved => true;

        public bool HasChanges => false;

        public UserSnapshot GetSnapshot()
            => UserSnapshot.Empty;

        public UserChanged GetChanges(DateTimeOffset timestamp)
            => null;

        public void ChangeUsername(Username username)
        {
        }

        public void Activate()
        {
        }

        public void Block()
        {
        }

        public void ChangeRole(UserRole role)
        {
        }

        public void ChangeName(Name name)
        {
        }

        public void ChangeEmail(Email email)
        {
        }

        public void ChangePhone(PhoneNumber phone)
        {
        }

        public void ChangePhoto(Photo photo)
        {
        }

        public void AddOrUpdateCard(CardId cardId, Money balance)
        {
        }

        public void RemoveCard(CardId cardId)
        {
        }

        public void BlockCard(CardId cardId)
        {
        }

        public void ActivateCard(CardId cardId)
        {
        }

        public void Remove()
        {
        }
    }
}