using System;
using Amethyst.Domain.ValueObjects;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Domain.ValueObjects;

namespace Ets.Services.Users.Domain.Contracts
{
    public interface IUser
    {
        long? CurrentVersion { get; }
        
        bool IsRemoved { get; }

        bool HasChanges { get; }

        UserSnapshot GetSnapshot();

        UserChanged GetChanges(DateTimeOffset timestamp);

        void ChangeUsername(Username username);

        void Activate();

        void Block();

        void ChangeRole(UserRole role);

        void ChangeName(Name name);

        void ChangeEmail(Email email);

        void ChangePhone(PhoneNumber phone);

        void ChangePhoto(Photo photo);

        void AddOrUpdateCard(CardId cardId, Money balance);

        void RemoveCard(CardId cardId);

        void BlockCard(CardId cardId);

        void ActivateCard(CardId cardId);

        void Remove();
    }
}