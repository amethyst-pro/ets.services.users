using System;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.ValueObjects;

namespace Ets.Services.Users.Domain.Contracts
{
    public interface IChangesService
    {
        UserChanged GetChanges(UserSnapshot snapshot, DateTimeOffset timestamp);
    }
}