using System;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.ValueObjects
{
    public readonly struct Profile : IEquatable<Profile>
    {
        public Profile(Name name, Email? email, PhoneNumber? phone, Photo? photo)
        {
            Name = name;
            Email = email;
            Phone = phone;
            Photo = photo;
        }
        
        public Name Name { get; }

        public Email? Email { get; }
        
        public PhoneNumber? Phone { get; }

        public Photo? Photo { get; }

        public Profile ChangeName(Name name)
            => new Profile(name, Email, Phone, Photo);
        
        public Profile ChangeEmail(Email email)
            => new Profile(Name, email, Phone, Photo);
        
        public Profile ChangePhone(PhoneNumber phone)
            => new Profile(Name, Email, phone, Photo);
        
        public Profile ChangePhoto(Photo photo)
            => new Profile(Name, Email, Phone, photo);

        public bool Equals(Profile other)
            => Name.Equals(other.Name) 
               && Email.Equals(other.Email) 
               && Phone.Equals(other.Phone) 
               && Photo.Equals(other.Photo);

        public override bool Equals(object obj)
            => obj is Profile other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Name.GetHashCode();
                hashCode = (hashCode * 397) ^ Email.GetHashCode();
                hashCode = (hashCode * 397) ^ Phone.GetHashCode();
                hashCode = (hashCode * 397) ^ Photo.GetHashCode();
                return hashCode;
            }
        }
    }
}