using System;
using System.Collections.Generic;
using Ets.Services.Users.Domain.Entities;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.ValueObjects
{
    public readonly struct UserSnapshot
    {
        public UserSnapshot(
            UserId id,
            Username username, 
            UserRole role, 
            UserState state, 
            Profile profile,
            IReadOnlyCollection<Card> cards,
            bool isAcceptAgreement, 
            bool isRemoved)
        {
            Id = id;
            Username = username;
            Role = role;
            State = state;
            Profile = profile;
            Cards = cards ?? Array.Empty<Card>();
            IsAcceptAgreement = isAcceptAgreement;
            IsRemoved = isRemoved;
        }

        public UserId Id { get; }
        
        public Username Username { get; }
        
        public UserRole Role { get; }
        
        public UserState State { get; }
        
        public Profile Profile { get; }

        public IReadOnlyCollection<Card> Cards { get; }
        
        public bool IsAcceptAgreement { get; }
        
        public bool IsRemoved { get; }
        
        public static UserSnapshot Empty => 
            new UserSnapshot(
                default,
                default,
                default, 
                default,
                default,
                default, 
                true, 
                true);
    }
}