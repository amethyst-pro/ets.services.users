using System;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.ValueObjects;

namespace Ets.Services.Users.Domain.Services
{
    public class ChangesService : IChangesService
    {
        public UserChanged GetChanges(UserSnapshot snapshot, DateTimeOffset timestamp)
            => new UserChanged(
                snapshot.Id, 
                snapshot.Username, 
                snapshot.Role, 
                snapshot.State, 
                timestamp);
    }
}