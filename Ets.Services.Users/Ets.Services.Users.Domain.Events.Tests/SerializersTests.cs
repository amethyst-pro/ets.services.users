using System;
using Amethyst.Domain;
using Amethyst.Microservices.Services.EventTest;
using Amethyst.Streams.Abstractions;
using Amethyst.Streams.Domain;
using AutoFixture;
using AutoFixture.Kernel;
using Ets.Services.Users.Domain.Events.Protobuf;
using FluentAssertions;
using SharpJuice.AutoFixture;
using Xunit;

namespace Ets.Services.Users.Domain.Events.Tests
{
    public sealed class SerializersTests
    {
        private readonly Fixture _fixture;
        private readonly IDomainEventSerializer _serializer;

        public SerializersTests()
        {
            _fixture = new Fixture();
            _serializer = DomainProtobufSerializer.Instance;
        }

        [Theory]
        [ClassData(typeof(AllDomainEvents<UserCreated>))]
        public void SerializingEvent_DeserializedEquivalentOne(Type t)
        {
            var @event = (IDomainEvent)new SpecimenContext(_fixture).Resolve(t);

            var eventData = _serializer.Serialize(@event, Guid.NewGuid());

            _fixture.CustomizeConstructor<RecordedEvent>(new
            {
                data = eventData.Data, 
                type = eventData.Type, 
                metadata = new byte[0]
            });
            var deserializedEvent = _serializer.Deserialize(_fixture.Create<RecordedEvent>());

            deserializedEvent.Should().BeEquivalentTo<object>(@event);
        }
    }
}