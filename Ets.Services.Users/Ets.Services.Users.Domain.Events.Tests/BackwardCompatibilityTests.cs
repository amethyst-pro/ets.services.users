using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amethyst.Domain;
using Amethyst.Microservices.Services.EventTest;
using Amethyst.Streams.Abstractions;
using Amethyst.Streams.Domain;
using AutoFixture;
using AutoFixture.Kernel;
using Ets.Services.Users.Domain.Events.Protobuf;
using FluentAssertions;
using Newtonsoft.Json;
using SharpJuice.AutoFixture;
using Xunit;

namespace Ets.Services.Users.Domain.Events.Tests
{
    public sealed class BackwardCompatibilityTests
    {
        private readonly IFixture _fixture = new Fixture();
        private readonly IDomainEventSerializer _serializer = DomainProtobufSerializer.Instance;
        
       [Fact(Skip = "manually")]
       public void CreateSerializedEvents_Success()
        {
            var data = new List<TestEventData>();
            var assembly = typeof(UserCreated).Assembly;
            var types = assembly.GetTypes().Where(t => typeof(IDomainEvent).IsAssignableFrom(t));
            
            foreach (var type in types)
            {
                var @event = (IDomainEvent) new SpecimenContext(_fixture).Resolve(type);
                var eventData = _serializer.Serialize(@event, Guid.Empty);
                
                data.Add(new TestEventData
                {
                    JsonType = type.FullName,
                    JsonData = JsonConvert.SerializeObject(@event),
                    ProtoType = eventData.Type,
                    ProtoData = eventData.Data
                });
            }

            var path = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.FullName;
            using (var writer = File.CreateText(Path.Combine(path, FileName.Value)))
            {
                new JsonSerializer().Serialize(writer, data);
            }
        }
        
        [Theory]
        [ClassData(typeof(AllDomainEvents<UserCreated>))]
        public void SerializingEvent_DeserializedEquivalentOne(Type type)
        {
            var @event = (IDomainEvent) new SpecimenContext(_fixture).Resolve(type);
            var eventData = _serializer.Serialize(@event, Guid.NewGuid());
            
            _fixture.CustomizeConstructor<RecordedEvent>(new
            {
                data = eventData.Data,
                type = eventData.Type,
                metadata = Array.Empty<byte>()
            });

            var actual = _serializer.Deserialize(_fixture.Create<RecordedEvent>());
            
            actual.Should().BeEquivalentTo<object>(@event);
        }

        [Theory]
        [ClassData(typeof(AllTestEvents))]
        public void SerializingEvent_Success(TestEventData eventData)
        {
            var jsonType = typeof(UserCreated).Assembly.GetType(eventData.JsonType);
            var jsonEvent = JsonConvert.DeserializeObject(eventData.JsonData, jsonType);

            var actual = _serializer.Deserialize(new RecordedEvent(
                default,
                default,
                default,
                eventData.ProtoType,
                DateTime.UtcNow,
                default,
                eventData.ProtoData,
                default));
            
            actual.Should().BeEquivalentTo(jsonEvent);
        }
    }
}