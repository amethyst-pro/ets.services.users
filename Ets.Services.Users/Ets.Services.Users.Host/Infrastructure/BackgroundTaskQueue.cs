using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Infrastructure
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue, IDisposable
    {
        private readonly ConcurrentQueue<(Func<CancellationToken, Task> task, string name)> _workItems = 
            new ConcurrentQueue<(Func<CancellationToken, Task> task, string name)>();
        
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(0);
        
        private readonly ILogger<BackgroundTaskQueue> _logger;

        public BackgroundTaskQueue(ILoggerFactory loggerFactory)
            => _logger = loggerFactory.CreateLogger<BackgroundTaskQueue>();

        public void QueueWorkItem(Func<CancellationToken, Task> workItem, string name = null)
        {
            if (workItem == null)
                throw new ArgumentNullException(nameof(workItem));

            var taskName = name ?? "No Name";
            _workItems.Enqueue((workItem, taskName));
            _logger.LogInformation($"Enqueued task '{taskName}'");
            _semaphore.Release();
        }

        public async Task<(Func<CancellationToken, Task> task, string name)> DequeueWorkItemAsync(
            CancellationToken cancellationToken)
        {
            await _semaphore.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }

        public void Dispose() => _semaphore.Dispose();
    }
}