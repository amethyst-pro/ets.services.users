using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ets.Services.Users.Host.Infrastructure
{
    public interface IBackgroundTaskQueue
    {
        void QueueWorkItem(Func<CancellationToken, Task> workItem, string name = null);

        Task<(Func<CancellationToken, Task> task, string name)> DequeueWorkItemAsync(CancellationToken stopToken);
    }
}