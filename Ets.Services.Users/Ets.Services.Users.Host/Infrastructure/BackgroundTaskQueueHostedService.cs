using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Infrastructure
{
    public sealed class BackgroundTaskQueueHostedService : BackgroundService
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly ILogger<BackgroundTaskQueueHostedService> _logger;

        public BackgroundTaskQueueHostedService(IBackgroundTaskQueue taskQueue, ILoggerFactory loggerFactory)
        {
            _taskQueue = taskQueue;
            _logger = loggerFactory.CreateLogger<BackgroundTaskQueueHostedService>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var (task, taskName) = await _taskQueue.DequeueWorkItemAsync(stoppingToken);
                if (task == null)
                {
                    break;
                }

                try
                {
                    _logger.LogInformation($"Executing task '{taskName}'");
                    var stopwatch = Stopwatch.StartNew();
                    await task.Invoke(stoppingToken);
                    _logger.LogInformation($"Task '{taskName}' completed, elapsed: {stopwatch.Elapsed}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error occurred executing '{taskName}'.");
                }
            }
        }
    }
}