using System;
using System.Threading.Tasks;
using Amethyst.Grpc.Protobuf.Converters;
using Ets.Services.Users.Application.Commands;
using Ets.Services.Users.Application.Contracts.Services;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Grpc;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Servers
{
    public sealed class UserServer : UserGrpc.UserGrpcBase
    {
        private readonly ILifetimeService _lifetimeService;
        private readonly IUserService _userService;
        private readonly ILogger<UserServer> _logger;
        
        public UserServer(
            ILifetimeService lifetimeService, 
            IUserService userService, 
            ILogger<UserServer> logger)
        {
            _lifetimeService = lifetimeService;
            _userService = userService;
            _logger = logger;
        }

        public override async Task<CommandResponse> Create(CreateRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(CreateRequest)}");

            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));

            if (!Enum.IsDefined(typeof(UserRole), (int)request.Role))
                throw new ArgumentException(nameof(request.Role));
            
            var userId = new UserId(request.UserId.ToGuid());
            var username = new Username(request.Username);
            var email = new Email(request.Email);

            var command = new CreateUser(userId, username, email, (UserRole)request.Role);

            var version = await _lifetimeService.CreateUserAsync(command);

            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> Remove(RemoveRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(RemoveRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            var userId = new UserId(request.UserId.ToGuid());
            
            var command = new RemoveUser(userId);

            var version = await _lifetimeService.RemoveAsync(command);
            
            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> ChangeUsername(ChangeUsernameRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangeUsernameRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            var userId = new UserId(request.UserId.ToGuid());
            var username = new Username(request.Username);
            
            var command = new ChangeUsername(userId, username);

            var version = await _userService.ChangeUsernameAsync(command);
            
            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> Activate(ActivateRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ActivateRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            var userId = new UserId(request.UserId.ToGuid());
            
            var command = new ActivateUser(userId);

            var version = await _userService.ActivateAsync(command);
            
            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> Block(BlockRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(BlockRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            var userId = new UserId(request.UserId.ToGuid());
            
            var command = new BlockUser(userId);

            var version = await _userService.BlockAsync(command);
            
            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> ChangeRole(ChangeRoleRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangeRoleRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            if (!Enum.IsDefined(typeof(UserRole), (int)request.Role))
                throw new ArgumentException(nameof(request.Role));
            
            var userId = new UserId(request.UserId.ToGuid());
            
            var command = new ChangeRole(userId, (UserRole)request.Role);

            var version = await _userService.ChangeRoleAsync(command);
            
            return new CommandResponse {Version = version};
        }
    }
}