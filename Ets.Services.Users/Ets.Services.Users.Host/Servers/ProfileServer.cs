using System;
using System.Threading.Tasks;
using Amethyst.Grpc.Protobuf.Converters;
using Ets.Services.Users.Application.Commands;
using Ets.Services.Users.Application.Contracts.Services;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Grpc;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Servers
{
    public sealed class ProfileServer : ProfileGrpc.ProfileGrpcBase
    {
        private readonly IProfileService _profileService;
        private readonly ILogger<ProfileServer> _logger;
        
        public ProfileServer(
            IProfileService profileService, 
            ILogger<ProfileServer> logger)
        {
            _profileService = profileService;
            _logger = logger;
        }

        public override async Task<CommandResponse> ChangeName(ChangeNameRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangeNameRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));

            var userId = new UserId(request.UserId.ToGuid());
            var name = new Name(request.FirstName, request.MiddleName, request.LastName);
            
            var command = new ChangeName(userId, name);

            var version = await _profileService.ChangeNameAsync(command);

            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> ChangeEmail(ChangeEmailRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangeEmailRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));

            var userId = new UserId(request.UserId.ToGuid());
            var email = new Email(request.Email);
            
            var command = new ChangeEmail(userId, email);

            var version = await _profileService.ChangeEmailAsync(command);
            
            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> ChangePhone(ChangePhoneRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangePhoneRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            var userId = new UserId(request.UserId.ToGuid());
            var phone = new PhoneNumber((CountryCode)request.Code, request.Number);
            
            var command = new ChangePhone(userId, phone);

            var version = await _profileService.ChangePhoneAsync(command);

            return new CommandResponse {Version = version};
        }

        public override async Task<CommandResponse> ChangePhoto(ChangePhotoRequest request, ServerCallContext context)
        {
            _logger.LogDebug($"Grpc request {nameof(ChangePhotoRequest)}");
            
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (request.UserId == null)
                throw new ArgumentNullException(nameof(request.UserId));
            
            if (request.Photo == null)
                throw new ArgumentNullException(nameof(request.Photo));
            
            var userId = new UserId(request.UserId.ToGuid());
            var photo = new Photo(request.Photo.ToByteArray());

            var command = new ChangePhoto(userId, photo);

            var version = await _profileService.ChangePhotoAsync(command);
            
            return new CommandResponse {Version = version};
        }
    }
}