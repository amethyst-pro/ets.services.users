using System.Collections.Generic;
using Amethyst.Domain;
using Amethyst.Streams.Domain;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events.ValueObjects;
using DomainUser = Ets.Services.Users.Domain.Aggregates.User;

namespace Ets.Services.Users.Host.Factories
{
    public class UserFactory : IAggregateFactory<DomainUser, UserId>
    {
        private readonly IChangesService _changesService;

        public UserFactory(IChangesService changesService)
            => _changesService = changesService;

        public DomainUser Create(UserId id, long version, IReadOnlyCollection<IDomainEvent> events)
            => new DomainUser(id, version, events, _changesService);
    }
}