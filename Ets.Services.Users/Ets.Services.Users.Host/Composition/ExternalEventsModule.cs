using Amethyst.Grpc.Protobuf;
using Amethyst.Pgsql.Publisher;
using Amethyst.Pgsql.Publishing;
using Autofac;
using Confluent.Kafka;
using Ets.Services.Users.Host.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Composition
{
    public sealed class ExternalEventsModule : Module
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _environment;
        private readonly ILoggerFactory _loggerFactory;

        public ExternalEventsModule(
            IConfiguration configuration, 
            IHostingEnvironment environment,
            ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _environment = environment;
            _loggerFactory = loggerFactory;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var brokers = _configuration.GetSection("Kafka:Brokers").Get<string>();

            var topic = "ets.users_userchanges";

            if (!_environment.IsProduction())
                topic += "_" + _environment.EnvironmentName.ToLowerInvariant();

            builder.RegisterInstance(new ExternalEventsService(
                    CreatePublisher(brokers, topic),
                    _loggerFactory.CreateLogger<ExternalEventsService>()))
                .AsImplementedInterfaces();
        }

        private IEventPublisher CreatePublisher(
            string kafkaServers,
            string topic)
        {
            return new EventPublisher(new ProducerSettings(
                    new ProducerConfig
                    {
                        Acks = Acks.All,
                        EnableIdempotence = true,
                        BootstrapServers = kafkaServers,
                        MessageSendMaxRetries = 5,
                        ClientId = "ets-users-changes",
                        EnableGaplessGuarantee = true,
                    },
                    topic),
                new RecordedEventSerializer(),
                _loggerFactory);
        }
    }
}