using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Host.Factories;
using Microsoft.AspNetCore.Hosting;
using DomainUser = Ets.Services.Users.Domain.Aggregates.User;

namespace Ets.Services.Users.Host.Composition
{
    public sealed class EventStoreSettings
    {
        public EventStoreSettings(IHostingEnvironment env, string serviceName)
        {
            Configuration = new DomainConfiguration(serviceName, env)
                .AddAggregate<DomainUser, UserId, UserFactory>(5);
        }

        public DomainConfiguration Configuration { get; }
    }
}