using System;
using System.Linq;
using Amethyst.Domain;
using Amethyst.Streams.Domain;
using Autofac;
using Ets.Services.Users.Domain.Events.Protobuf;

namespace Ets.Services.Users.Host.Composition
{
    public sealed class RepositoryModule : Module
    {
        private readonly object[] _factories;

        public RepositoryModule(DomainConfiguration configuration)
        {
            var factories = configuration.GetFactories().ToArray();
            
            if (factories.Length == 0)
                throw new InvalidOperationException("There is no aggregates");

            _factories = factories;
        }

        protected override void Load(ContainerBuilder builder)
        {
            foreach (var factory in _factories)
            {
                if (factory is Type type)
                {
                    builder.RegisterType(type)
                        .InstancePerLifetimeScope()
                        .AsImplementedInterfaces();
                }
                else
                {
                    builder.RegisterInstance(factory)
                        .AsImplementedInterfaces()
                        .SingleInstance();
                }
            }

            builder.RegisterGeneric(typeof(AggregateRepository<,>))
                .InstancePerLifetimeScope()
                .As(typeof(IRepository<,>));

            builder.RegisterType<StreamResolver>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterInstance(DomainProtobufSerializer.Instance)
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}