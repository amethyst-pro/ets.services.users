using System;
using Amethyst.Grpc.Protobuf;
using Amethyst.Subscription.Broker;
using Amethyst.Subscription.Hosting;
using Amethyst.Subscription.Hosting.Autofac;
using Autofac;
using Ets.Services.Users.Application.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Ets.Services.Users.Host.Composition
{
    public class SubscriptionModule : Module
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _environment;
        private readonly string _groupId;

        public SubscriptionModule(
            IConfiguration configuration,
            IHostingEnvironment environment,
            string groupId)
        {
            _configuration = configuration;
            _environment = environment;
            _groupId = groupId;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var endpointConfig = new EndpointConfiguration();

            AddCardsSubscriptions(endpointConfig);

            builder.RegisterModule(new SubscriptionEndpointModule(
                endpointConfig,
                typeof(CardService).Assembly));
        }
        
        private void AddCardsSubscriptions(EndpointConfiguration endpointConfig)
        {
            var brokers = _configuration.GetSection("Kafka:Brokers").Get<string>();
            var serializer = new DomainEventDeserializer(Cards.Domain.Events.Protobuf.DomainProtobufSerializer.Instance);

            endpointConfig
                .AddSubscription(2, GetSettings("CardsConsumer"), serializer);

            ConsumerSettings GetSettings(string section)
            {
                var settings = _configuration.GetSection(section).Get<ConsumerSettings>();

                if (!_environment.IsProduction())
                    settings.Topic += "_" + _environment.EnvironmentName.ToLowerInvariant();

                settings.Config.GroupId = _groupId;
                settings.Config.BootstrapServers = brokers;
                settings.Config.MaxPollIntervalMs = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
                settings.Config.SessionTimeoutMs = (int) TimeSpan.FromMinutes(1).TotalMilliseconds;

                return settings;
            }
        }
    }
}