using Ets.Services.Users.Application.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Services.Users.Host.Composition
{
    public static class MessageHandlersExtensions
    {
        public static IServiceCollection AddBlockMessageHandler(this IServiceCollection services)
            => services.AddSingleton<BlockMessageHandler>();
    }
}