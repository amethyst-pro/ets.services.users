using Amethyst.Grpc.Extensions;
using Amethyst.Microservices.Services;
using Amethyst.Microservices.Services.Interceptors;
using Amethyst.Platform.Grpc.Extensions;
using Amethyst.Streams.Metadata.Abstractions;
using Ets.Services.Users.Host.Configurations;
using Ets.Services.Users.Host.Servers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Services.Users.Host.Composition
{
    public static class GrpcServersExtensions
    {
        public static IServiceCollection AddGrpcServers(
            this IServiceCollection services, 
            IConfiguration configuration)
        {
            services.AddPlatformGrpc();
            services.AddSingleton<IMetadataContext, ServiceMetadataContext>();
            services.AddTransient<GrpcInterceptor>();

            services.AddGrpcServer(server =>
            {
                var config = configuration.GetSection("GrpcServer").Get<GrpcServerConfiguration>();
                server.AddPort(config.Host, config.Port);
                
                server.AddService<UserServer>();
                server.AddService<ProfileServer>();

                server.AddInterceptor<GrpcInterceptor>();
                server.AddReflection();
            });

            return services;
        }
    }
}