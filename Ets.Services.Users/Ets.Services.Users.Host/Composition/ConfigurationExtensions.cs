using Amethyst.Subscription.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Services.Users.Host.Composition
{
    public static class ConfigurationExtensions
    {
        public static IServiceCollection AddConfigurations(
            this IServiceCollection services,
            IConfiguration configuration)
            => services.Configure<EventObserverConfiguration>(configuration.GetSection("EventObserver"));
    }
}