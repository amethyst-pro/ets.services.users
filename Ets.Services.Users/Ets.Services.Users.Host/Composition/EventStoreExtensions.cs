using Amethyst.Pgsql;
using Amethyst.Pgsql.Configuring;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Composition
{
    public static class EventStoreExtensions
    {
        public static IServiceCollection ConfigureEventStore(
            this IServiceCollection services,
            IConfiguration configuration,
            EventStoreSettings eventStoreSettings,
            ILoggerFactory loggerFactory)
        {
            var esConnectionString = configuration.GetSection("Postgres").Value;
            services.AddEventStore(
                new PgsqlEventStoreOptions {ConnectionString = esConnectionString},
                loggerFactory,
                eventStoreSettings.Configuration.CreateEventStoreConfiguration(
                    configuration.GetSection("Kafka:Brokers").Get<string>(),
                    loggerFactory
                ));

            return services;
        }
    }
}