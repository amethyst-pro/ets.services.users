using System;
using System.Collections.Generic;
using System.Linq;
using Amethyst.Domain;
using Amethyst.Domain.Abstractions;
using Amethyst.Grpc.Protobuf;
using Amethyst.Pgsql.Configuring;
using Amethyst.Pgsql.Publisher;
using Amethyst.Pgsql.Publishing;
using Amethyst.Streams.Abstractions;
using Amethyst.Streams.Domain;
using Confluent.Kafka;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Composition
{
    public sealed class DomainConfiguration
    {
           private readonly string _serviceFullName;
        private readonly IHostingEnvironment _environment;

        private readonly List<(Type type, object factory, int partitions)> _aggregates =
            new List<(Type, object, int)>();

        private readonly IRecordedEventSerializer _recordedEventSerializer
            = new RecordedEventSerializer();

        public DomainConfiguration(string serviceFullName, IHostingEnvironment environment)
        {
            _serviceFullName = serviceFullName;
            _environment = environment;
        }

        public DomainConfiguration AddAggregate<T, TId>(
            Func<TId, long, IReadOnlyCollection<IDomainEvent>, T> factory,
            int partitions = 1)
            where T : IAggregate<TId>
            where TId : IGuidId
        {
            _aggregates.Add((typeof(T), new AggregateFactory<T, TId>(factory), partitions));
            return this;
        }
        
        public DomainConfiguration AddAggregate<T, TId, TFactory>(
            int partitions = 1)
            where T : IAggregate<TId>
            where TId : IGuidId
            where TFactory : IAggregateFactory<T, TId>
        {
            _aggregates.Add((typeof(T), typeof(TFactory), partitions));
            return this;
        }

        public EventStoreConfiguration CreateEventStoreConfiguration(
            string kafkaServers,
            ILoggerFactory loggerFactory)
        {
            var config = new EventStoreConfiguration();

            _aggregates.ForEach(a =>
            {
                if (a.partitions > 1)
                {
                    config.AddCategoryType(a.type, a.partitions,
                        CreatePublisher(a.type, kafkaServers, loggerFactory));
                }
                else
                {
                    config.AddCategoryType(a.type,
                        CreatePublisher(a.type, kafkaServers, loggerFactory));
                }
            });

            return config;
        }

        public IEnumerable<object> GetFactories() => _aggregates.Select(a => a.factory);

        public string GetSubscriptionTopicFor(Type aggregateType)
        {
            var topic = $"amethyst.{_serviceFullName}_{aggregateType.Name.ToLowerInvariant()}";
            if (_environment.IsProduction())
                return topic;

            return topic + "_" + _environment.EnvironmentName.ToLowerInvariant();
        }

        private IEventPublisher CreatePublisher(
            Type aggregateType,
            string kafkaServers,
            ILoggerFactory loggerFactory)
        {
            var ack = _environment.IsProduction()
                ? Acks.All
                : Acks.Leader;
            
            return new EventPublisher(
                new ProducerSettings(
                    new ProducerConfig
                    {
                        Acks = ack,
                        EnableIdempotence = ack == Acks.All,
                        MaxInFlight = 1, 
                        BootstrapServers = kafkaServers,
                        MessageSendMaxRetries = 5,
                        ClientId = "amethyst." + _serviceFullName,
                        EnableGaplessGuarantee = ack == Acks.All,
                    }, GetSubscriptionTopicFor(aggregateType)),
                _recordedEventSerializer,
                loggerFactory);
        }
    }
}