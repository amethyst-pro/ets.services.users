using Ets.Services.Users.Host.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Services.Users.Host.Composition
{
    public static class BackgroundTasksExtensions
    {
        public static IServiceCollection AddBackgrounTasks(this IServiceCollection services)
            => services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>()
                .AddHostedService<BackgroundTaskQueueHostedService>();
    }
}