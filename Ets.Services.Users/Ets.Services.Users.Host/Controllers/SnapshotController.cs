using System;
using System.Threading.Tasks;
using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Ets.Services.Users.Domain.ValueObjects;
using Microsoft.AspNetCore.Mvc;
using DomainUser = Ets.Services.Users.Domain.Aggregates.User;

namespace Ets.Services.Users.Host.Controllers
{
    [Route("api/snapshot/users/{userId}")]
    [ApiController]
    public sealed class SnapshotController : ControllerBase
    {
        private readonly IRepository<DomainUser, UserId> _repository;

        public SnapshotController(IRepository<DomainUser, UserId> repository)
            =>  _repository = repository;

        [HttpGet]
        public async Task<ActionResult<UserSnapshot>> GetEvents(Guid userId)
        {
            var id = new UserId(userId);
            
            var user = await _repository.GetAsync(id);

            if (user.Any())
                return user.Single().GetSnapshot();

            return NoContent();
        }
    }
}