﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amethyst.Grpc.Protobuf;
using Amethyst.Streams;
using Amethyst.Streams.Abstractions;
using Amethyst.Streams.Domain;
using Amethyst.Streams.Metadata.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Ets.Services.Users.Host.Controllers
{
    [Route("api/insights/users/{userId}")]
    [ApiController]
    public class InsightsController : ControllerBase
    {
        private const string Category = "User";
        private readonly IEventStore _store;
        private readonly IDomainEventSerializer _serializer;

        private static readonly Dictionary<string, string> ShortNames =
            new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"user", nameof(User)},
            };

        public InsightsController(IEventStore store, IDomainEventSerializer serializer)
        {
            _store = store;
            _serializer = serializer;
        }

        [HttpGet]
        public async Task<IActionResult> GetEvents(Guid userId)
        {
            
            var resolvedCategory = ShortNames.GetValueOrDefault(Category, Category);

            var stream = new Stream(new StreamId(resolvedCategory, userId), _store, _serializer,
                NullMetadataContext.Instance);

            var eventsResult = await stream.ReadEventsForward();

            return Ok(eventsResult.Events.Select((e, i) => new { Num = i, Type = e.GetType().Name, Event = e}));
        }
        
        [HttpGet("raw")]
        public async Task<IActionResult> GetRawEvents(Guid userId)
        {
            var serializer = new MetadataSerializer();
            
            var resolvedCategory = ShortNames.GetValueOrDefault(Category, Category);

            var eventsResult = await _store.ReadStreamEventsForwardAsync(new StreamId(resolvedCategory, userId), 0);

            return Ok(eventsResult.Events.Select(p => new
            {
                p.Id, p.Type, p.Number, p.StreamId, p.Created, p.CreatedEpoch,
                Metadata = serializer.Deserialize(p.Metadata)
            }));
        }
    }
}