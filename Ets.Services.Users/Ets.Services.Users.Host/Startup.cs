﻿using System;
using Amethyst.Subscription.Abstractions;
using Autofac;
using Ets.Services.Users.Application.Contracts.Repositories;
using Ets.Services.Users.Application.Ports.Repositories;
using Ets.Services.Users.Application.Services;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Services;
using Ets.Services.Users.Host.Composition;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SharpJuice.Essentials;

namespace Ets.Services.Users.Host
{
    public class Startup
    {
        private const string ServiceName = "users";
        
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;
        private static IHostingEnvironment _environment;
        private readonly EventStoreSettings _eventStoreSettings;
        
        public Startup(
            IConfiguration configuration, 
            ILoggerFactory loggerFactory, 
            IHostingEnvironment environment)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
            _environment = environment ?? throw new ArgumentNullException(nameof(_environment)); 
            _eventStoreSettings = new EventStoreSettings(environment, ServiceName);
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureEventStore(_configuration, _eventStoreSettings, _loggerFactory);
            services.AddGrpcServers(_configuration);
            services.AddConfigurations(_configuration);
            services.AddBlockMessageHandler();
            services.AddBackgrounTasks();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseMvc();
        }
        
        public void ConfigureContainer(ContainerBuilder builder)
        {
            var applicationAssembly = typeof(UserService).Assembly;
            
            var groupId = "amethyst-" + ServiceName;
            if (!_environment.IsProduction())
                groupId += "-" + _environment.EnvironmentName;

            builder.RegisterAssemblyTypes(applicationAssembly)
                .Where(t => !t.IsClosedTypeOf(typeof(IEventHandler<>)))
                .Where(t => t.Name.EndsWith("Service") || t.Name.EndsWith("Factory"))
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<LifetimeService>()
                .InstancePerLifetimeScope()
                .AsSelf()
                .AsImplementedInterfaces();
            
            builder.RegisterType<Clock>().As<IClock>();
            builder.RegisterType<ChangesService>().As<IChangesService>();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();

            builder.RegisterModule(new ExternalEventsModule(_configuration, _environment, _loggerFactory));
            builder.RegisterModule(new SubscriptionModule(_configuration, _environment, groupId));
            builder.RegisterModule(new RepositoryModule(_eventStoreSettings.Configuration));
        }

    }
}