﻿using Amethyst.Platform;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Ets.Services.Users.Host
{
    public static class Program
    {
        public static void Main(string[] args)
            => CreateWebHostBuilder(args)
                .UsePlatform()
                .ConfigureServices(services => services.AddAutofac())
                .Build()
                .Run();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}