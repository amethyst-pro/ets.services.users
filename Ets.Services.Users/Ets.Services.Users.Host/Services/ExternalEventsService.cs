using System;
using System.Threading.Tasks;
using Amethyst.Pgsql.Publishing;
using Amethyst.Streams.Abstractions;
using Ets.Services.Users.Application.Contracts.Services;
using Ets.Services.Users.Domain.Events;
using Ets.Services.Users.Domain.Events.Protobuf;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Host.Services
{
    public class ExternalEventsService : IExternalEventsService
    {
        private readonly IEventPublisher _publisher;
        private readonly ILogger<ExternalEventsService> _logger;

        public ExternalEventsService(IEventPublisher publisher, ILogger<ExternalEventsService> logger)
        {
            _publisher = publisher ?? throw new ArgumentNullException(nameof(publisher));
            _logger = logger;
        }

        public async Task SendAsync(UserChanged changed)
        {
            if (changed == null)
                return;
            
            var serializedEvent = DomainProtobufSerializer.Instance.Serialize(changed, Guid.NewGuid());
            var streamId = new StreamId("UserChanged", changed.UserId.Value);

            var message = new RecordedEvent(
                serializedEvent.Id,
                streamId,
                0,
                serializedEvent.Type,
                changed.Timestamp.DateTime,
                changed.Timestamp.ToUnixTimeMilliseconds(),
                serializedEvent.Data,
                serializedEvent.Metadata);

            await _publisher.PublishAsync(new[] {message});

            _logger.LogInformation($"UserChanged {changed.UserId} sent. {{@changed}}.", changed);
        }
    }
}