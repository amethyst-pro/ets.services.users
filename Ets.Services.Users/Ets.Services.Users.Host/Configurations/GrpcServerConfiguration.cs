namespace Ets.Services.Users.Host.Configurations
{
    public sealed class GrpcServerConfiguration
    {
        public string Host { get; set; }
        
        public int Port { get; set; }
    }
}