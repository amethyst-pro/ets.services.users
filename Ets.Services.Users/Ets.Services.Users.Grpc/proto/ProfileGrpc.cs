// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: proto/profile.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Ets.Services.Users.Grpc {
  public static partial class ProfileGrpc
  {
    static readonly string __ServiceName = "ets.services.users.grpc.ProfileGrpc";

    static readonly grpc::Marshaller<global::Ets.Services.Users.Grpc.ChangeNameRequest> __Marshaller_ets_services_users_grpc_ChangeNameRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Ets.Services.Users.Grpc.ChangeNameRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Ets.Services.Users.Grpc.CommandResponse> __Marshaller_ets_services_users_grpc_CommandResponse = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Ets.Services.Users.Grpc.CommandResponse.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Ets.Services.Users.Grpc.ChangeEmailRequest> __Marshaller_ets_services_users_grpc_ChangeEmailRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Ets.Services.Users.Grpc.ChangeEmailRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Ets.Services.Users.Grpc.ChangePhoneRequest> __Marshaller_ets_services_users_grpc_ChangePhoneRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Ets.Services.Users.Grpc.ChangePhoneRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Ets.Services.Users.Grpc.ChangePhotoRequest> __Marshaller_ets_services_users_grpc_ChangePhotoRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Ets.Services.Users.Grpc.ChangePhotoRequest.Parser.ParseFrom);

    static readonly grpc::Method<global::Ets.Services.Users.Grpc.ChangeNameRequest, global::Ets.Services.Users.Grpc.CommandResponse> __Method_ChangeName = new grpc::Method<global::Ets.Services.Users.Grpc.ChangeNameRequest, global::Ets.Services.Users.Grpc.CommandResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ChangeName",
        __Marshaller_ets_services_users_grpc_ChangeNameRequest,
        __Marshaller_ets_services_users_grpc_CommandResponse);

    static readonly grpc::Method<global::Ets.Services.Users.Grpc.ChangeEmailRequest, global::Ets.Services.Users.Grpc.CommandResponse> __Method_ChangeEmail = new grpc::Method<global::Ets.Services.Users.Grpc.ChangeEmailRequest, global::Ets.Services.Users.Grpc.CommandResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ChangeEmail",
        __Marshaller_ets_services_users_grpc_ChangeEmailRequest,
        __Marshaller_ets_services_users_grpc_CommandResponse);

    static readonly grpc::Method<global::Ets.Services.Users.Grpc.ChangePhoneRequest, global::Ets.Services.Users.Grpc.CommandResponse> __Method_ChangePhone = new grpc::Method<global::Ets.Services.Users.Grpc.ChangePhoneRequest, global::Ets.Services.Users.Grpc.CommandResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ChangePhone",
        __Marshaller_ets_services_users_grpc_ChangePhoneRequest,
        __Marshaller_ets_services_users_grpc_CommandResponse);

    static readonly grpc::Method<global::Ets.Services.Users.Grpc.ChangePhotoRequest, global::Ets.Services.Users.Grpc.CommandResponse> __Method_ChangePhoto = new grpc::Method<global::Ets.Services.Users.Grpc.ChangePhotoRequest, global::Ets.Services.Users.Grpc.CommandResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ChangePhoto",
        __Marshaller_ets_services_users_grpc_ChangePhotoRequest,
        __Marshaller_ets_services_users_grpc_CommandResponse);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Ets.Services.Users.Grpc.ProfileReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of ProfileGrpc</summary>
    [grpc::BindServiceMethod(typeof(ProfileGrpc), "BindService")]
    public abstract partial class ProfileGrpcBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Ets.Services.Users.Grpc.CommandResponse> ChangeName(global::Ets.Services.Users.Grpc.ChangeNameRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Ets.Services.Users.Grpc.CommandResponse> ChangeEmail(global::Ets.Services.Users.Grpc.ChangeEmailRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhone(global::Ets.Services.Users.Grpc.ChangePhoneRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhoto(global::Ets.Services.Users.Grpc.ChangePhotoRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for ProfileGrpc</summary>
    public partial class ProfileGrpcClient : grpc::ClientBase<ProfileGrpcClient>
    {
      /// <summary>Creates a new client for ProfileGrpc</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public ProfileGrpcClient(grpc::Channel channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for ProfileGrpc that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public ProfileGrpcClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected ProfileGrpcClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected ProfileGrpcClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangeName(global::Ets.Services.Users.Grpc.ChangeNameRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangeName(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangeName(global::Ets.Services.Users.Grpc.ChangeNameRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ChangeName, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangeNameAsync(global::Ets.Services.Users.Grpc.ChangeNameRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangeNameAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangeNameAsync(global::Ets.Services.Users.Grpc.ChangeNameRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ChangeName, null, options, request);
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangeEmail(global::Ets.Services.Users.Grpc.ChangeEmailRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangeEmail(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangeEmail(global::Ets.Services.Users.Grpc.ChangeEmailRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ChangeEmail, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangeEmailAsync(global::Ets.Services.Users.Grpc.ChangeEmailRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangeEmailAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangeEmailAsync(global::Ets.Services.Users.Grpc.ChangeEmailRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ChangeEmail, null, options, request);
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangePhone(global::Ets.Services.Users.Grpc.ChangePhoneRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangePhone(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangePhone(global::Ets.Services.Users.Grpc.ChangePhoneRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ChangePhone, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhoneAsync(global::Ets.Services.Users.Grpc.ChangePhoneRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangePhoneAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhoneAsync(global::Ets.Services.Users.Grpc.ChangePhoneRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ChangePhone, null, options, request);
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangePhoto(global::Ets.Services.Users.Grpc.ChangePhotoRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangePhoto(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Ets.Services.Users.Grpc.CommandResponse ChangePhoto(global::Ets.Services.Users.Grpc.ChangePhotoRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ChangePhoto, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhotoAsync(global::Ets.Services.Users.Grpc.ChangePhotoRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return ChangePhotoAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Ets.Services.Users.Grpc.CommandResponse> ChangePhotoAsync(global::Ets.Services.Users.Grpc.ChangePhotoRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ChangePhoto, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override ProfileGrpcClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new ProfileGrpcClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(ProfileGrpcBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_ChangeName, serviceImpl.ChangeName)
          .AddMethod(__Method_ChangeEmail, serviceImpl.ChangeEmail)
          .AddMethod(__Method_ChangePhone, serviceImpl.ChangePhone)
          .AddMethod(__Method_ChangePhoto, serviceImpl.ChangePhoto).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, ProfileGrpcBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_ChangeName, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Ets.Services.Users.Grpc.ChangeNameRequest, global::Ets.Services.Users.Grpc.CommandResponse>(serviceImpl.ChangeName));
      serviceBinder.AddMethod(__Method_ChangeEmail, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Ets.Services.Users.Grpc.ChangeEmailRequest, global::Ets.Services.Users.Grpc.CommandResponse>(serviceImpl.ChangeEmail));
      serviceBinder.AddMethod(__Method_ChangePhone, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Ets.Services.Users.Grpc.ChangePhoneRequest, global::Ets.Services.Users.Grpc.CommandResponse>(serviceImpl.ChangePhone));
      serviceBinder.AddMethod(__Method_ChangePhoto, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Ets.Services.Users.Grpc.ChangePhotoRequest, global::Ets.Services.Users.Grpc.CommandResponse>(serviceImpl.ChangePhoto));
    }

  }
}
#endregion
