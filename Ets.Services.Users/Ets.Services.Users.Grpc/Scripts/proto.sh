#!/usr/bin/env bash
echo "Generation in progress.."

# protoc path
PROTOC=~/".nuget/packages/grpc.tools/1.17.1/tools/macosx_x64/protoc"
echo Protoc: $PROTOC

PLUGIN="${HOME}/.nuget/packages/grpc.tools/1.17.1/tools/macosx_x64/grpc_csharp_plugin"
echo Plugin: $PLUGIN

# imports *.proto path
IMPORTS="../protos"
echo Imports: $IMPORTS

# *.proto file path
FILE="../protos/users.proto"
echo File: $FILE

# messages
CSHARP_OUT="../"
echo Messages: $CSHARP_OUT

#transport
GRPC_OUT="../"
echo Proxy: $GRPC_OUT

#command
$PROTOC -I=$IMPORTS --csharp_out $CSHARP_OUT --grpc_out $GRPC_OUT $FILE --plugin=protoc-gen-grpc=$PLUGIN