using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class PhotoChanged : IDomainEvent
    {
        public PhotoChanged(UserId userId, Photo photo)
        {
            UserId = userId;
            Photo = photo;
        }

        public UserId UserId { get; }
        
        public Photo Photo { get; }
    }
}