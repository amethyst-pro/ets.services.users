using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class EmailChanged : IDomainEvent
    {
        public EmailChanged(UserId userId, Email email)
        {
            UserId = userId;
            Email = email;
        }

        public UserId UserId { get; }
        
        public Email Email { get; }
    }
}