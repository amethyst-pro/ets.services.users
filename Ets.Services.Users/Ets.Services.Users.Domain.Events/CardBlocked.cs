using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class CardBlocked : IDomainEvent
    {
        public CardBlocked(UserId userId, CardId cardId)
        {
            UserId = userId;
            CardId = cardId;
        }

        public UserId UserId { get; }
        
        public CardId CardId { get; }
    }
}