using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UserBlocked : IDomainEvent
    {
        public UserBlocked(UserId userId)
            => UserId = userId;

        public UserId UserId { get; }
    }
}