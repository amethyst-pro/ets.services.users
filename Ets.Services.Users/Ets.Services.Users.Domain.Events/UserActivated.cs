using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UserActivated : IDomainEvent
    {
        public UserActivated(UserId userId)
            => UserId = userId;

        public UserId UserId { get; }
    }
}