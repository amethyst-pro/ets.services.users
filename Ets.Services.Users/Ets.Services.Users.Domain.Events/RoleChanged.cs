using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class RoleChanged : IDomainEvent
    {
        public RoleChanged(UserId userId, UserRole role)
        {
            UserId = userId;
            Role = role;
        }

        public UserId UserId { get; }
        
        public UserRole Role { get; }
    }
}