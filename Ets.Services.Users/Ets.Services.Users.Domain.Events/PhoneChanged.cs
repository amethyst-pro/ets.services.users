using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class PhoneChanged : IDomainEvent
    {
        public PhoneChanged(UserId userId, PhoneNumber phone)
        {
            UserId = userId;
            Phone = phone;
        }

        public UserId UserId { get; }
        
        public PhoneNumber Phone { get; }
    }
}