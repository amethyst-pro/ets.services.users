namespace Ets.Services.Users.Domain.Events.Primitives
{
    public enum UserState
    {
        None = 0,
        NotActive,
        Active,
        Blocked
    }
}