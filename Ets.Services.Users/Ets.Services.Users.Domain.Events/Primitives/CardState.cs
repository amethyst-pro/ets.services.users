namespace Ets.Services.Users.Domain.Events.Primitives
{
    public enum CardState
    {
        None = 0,
        Active,
        Blocked,
        Expired
    }
}