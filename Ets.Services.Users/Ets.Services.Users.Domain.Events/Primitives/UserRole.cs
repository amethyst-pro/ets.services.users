namespace Ets.Services.Users.Domain.Events.Primitives
{
    public enum UserRole
    {
        None = 0,
        Admin,
        Customer
    }
}