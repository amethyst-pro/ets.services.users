using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class NameChanged : IDomainEvent
    {
        public NameChanged(UserId userId, Name name)
        {
            UserId = userId;
            Name = name;
        }

        public UserId UserId { get; }
        
        public Name Name { get; }
    }
}