using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UserRemoved : IDomainEvent
    {
        public UserRemoved(UserId userId)
            => UserId = userId;

        public UserId UserId { get; }
    }
}