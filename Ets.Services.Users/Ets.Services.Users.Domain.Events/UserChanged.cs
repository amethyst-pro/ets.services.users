using System;
using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UserChanged : IDomainEvent
    {
        public UserChanged(
            UserId userId,
            Username username, 
            UserRole role, 
            UserState state, DateTimeOffset timestamp)
        {
            UserId = userId;
            Username = username;
            Role = role;
            State = state;
            Timestamp = timestamp;
        }

        public UserId UserId { get; }
        
        public Username Username { get; }
        
        public UserRole Role { get; }
        
        public UserState State { get; }
        
        public DateTimeOffset Timestamp { get; }
    }
}