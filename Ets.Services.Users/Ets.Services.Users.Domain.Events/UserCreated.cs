using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UserCreated : IDomainEvent
    {
        public UserCreated(
            UserId userId,
            Username username, 
            Email email,
            UserRole role)
        {
            UserId = userId;
            Username = username;
            Email = email;
            Role = role;
        }
        
        public UserId UserId { get; }

        public Username Username { get; }
        
        public Email Email { get; }
        
        public UserRole Role { get; }

        public UserState State => UserState.NotActive;
        
        public bool isAcceptAgreement => true;
    }
}