using System;

namespace Ets.Services.Users.Domain.Events.ValueObjects
{
    public readonly struct Username : IEquatable<Username>
    {
        public Username(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            
            if (value.Length < 4)
                throw new ArgumentException($"Username less then 4 chars.",nameof(value));
            
            Value = value;
        }

        public string Value { get; }

        public bool Equals(Username other)
            => string.Equals(Value, other.Value);

        public override bool Equals(object obj)
            =>  obj is Username other && Equals(other);

        public override int GetHashCode()
            => Value != null 
                ? Value.GetHashCode()
                : 0;
    }
}