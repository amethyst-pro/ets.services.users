using System;

namespace Ets.Services.Users.Domain.Events.ValueObjects
{
    public readonly struct Name : IEquatable<Name>
    {
        public Name(string first, string middle, string last)
        {
            if (string.IsNullOrWhiteSpace(first))
                throw new ArgumentException("First name not specified.");

            First = first;
            Middle = middle;
            Last = last;
        }

        public string First { get; }
        
        public string Middle { get; }
        
        public string Last { get; }

        public bool Equals(Name other)
            => string.Equals(First, other.First) 
               && string.Equals(Middle, other.Middle) 
               && string.Equals(Last, other.Last);

        public override bool Equals(object obj)
            => obj is Name other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (First != null ? First.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Middle != null ? Middle.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Last != null ? Last.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}