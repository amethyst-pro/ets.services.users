using System;
using Ets.Services.Users.Domain.Events.Primitives;

namespace Ets.Services.Users.Domain.Events.ValueObjects
{
    public readonly struct PhoneNumber : IEquatable<PhoneNumber>
    {
        public PhoneNumber(CountryCode code, string number)
        {
            Code = code;
            Number = number;
        }

        public CountryCode Code { get; }

        public string Number { get; }

        public bool Equals(PhoneNumber other)
            => Code == other.Code 
               && string.Equals(Number, other.Number);

        public override bool Equals(object obj)
            => obj is PhoneNumber other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) Code * 397) ^ (Number != null ? Number.GetHashCode() : 0);
            }
        }
    }
}