using System;
using System.Collections.Generic;
using System.Linq;

namespace Ets.Services.Users.Domain.Events.ValueObjects
{
    public readonly struct Photo : IEquatable<Photo>
    {
        public Photo(IReadOnlyCollection<byte> value)
            => Value = value ?? throw new ArgumentNullException(nameof(value));

        public IReadOnlyCollection<byte> Value { get; }

        public bool Equals(Photo other)
            => Value.SequenceEqual(other.Value);

        public override bool Equals(object obj)
            => obj is Photo other && Equals(other);

        public override int GetHashCode()
            => Value != null ? Value.GetHashCode() : 0;
    }
}