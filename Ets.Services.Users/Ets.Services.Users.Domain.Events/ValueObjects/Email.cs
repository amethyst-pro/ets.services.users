using System;

namespace Ets.Services.Users.Domain.Events.ValueObjects
{
    public readonly struct Email : IEquatable<Email>
    {
        public Email(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            
            //TODO: Реализовать проверку Regex
            
            Value = value;
        }

        public string Value { get; }

        public bool Equals(Email other)
            => string.Equals(Value, other.Value);

        public override bool Equals(object obj)
            => obj is Email other && Equals(other);

        public override int GetHashCode()
            => Value != null ? Value.GetHashCode() : 0;
    }
}