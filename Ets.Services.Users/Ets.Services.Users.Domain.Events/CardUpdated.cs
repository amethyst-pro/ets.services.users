using Amethyst.Domain;
using Amethyst.Domain.ValueObjects;
using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class CardUpdated : IDomainEvent
    {
        public CardUpdated(UserId userId, CardId cardId, Money balance, CardState state)
        {
            UserId = userId;
            CardId = cardId;
            Balance = balance;
            State = state;
        }

        public UserId UserId { get; }
        
        public CardId CardId { get; }
        
        public Money Balance { get; }
        
        public CardState State { get; }
    }
}