using Amethyst.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events
{
    public sealed class UsernameChanged : IDomainEvent
    {
        public UsernameChanged(UserId userId, Username username)
        {
            UserId = userId;
            Username = username;
        }
        
        public UserId UserId { get; }

        public Username Username { get; }
    }
}