# Ets.Services.Users

Микросервис пользователей из демонстрационного пакета event sourcing платформы Amethyst.

## Описание

Демонстрационный микросервис для управления пользователями. Получает команды по протоколу gRPC от Identity Server и Gateway, 
подписан на события микросервиса Ets.Services.Cards.
При изменении состояния своего агрегата генерирует события и добавляет их в InMemory-коллекцию для последующего 
атомарного сохранения в хранилище Amethyst и публикации на шину обмена данными.

## Методологии

* DDD
* Event Sourcing
* gRPC
* \(Optional) Apache Kafka
* \(Optional) PostgreSQL

## Технологический стек

* C# 7.3
* .NET Core 2.2

## Слои сервиса

0. **Host.** Хост микросервиса, точка входа, подключение зависимостей.
0. **Grpc.** Объявление gRPC-контрактов микросервиса, клиент для его вызова.
0. **Application.** Сервисы инкапсулирующие бизнес-логику вызова агрегата и работу с репозиториями.
0. **Domain.** Доменная модель микросервиса и контракты агрегатов.
0. **Events.** События генерируемые микросервисом.
0. **Events.Protobuf.** Библиотека для protobuf сериализации событий микросервиса

## Генерируемые события 

#### User (aggregate root)

- CardActivated
- CardBlocked
- CardRemoved
- CardUpdated


- EmailChanged
- NameChanged
- PhoneChanged
- PhotoChanged


- RoleChanged
- UserActivated
- UserBlocked
- UserCreated
- UsernameChanged
- UserRemoved

#### External

- UserChanged

## Обрабатываемые события

#### События сервиса Ets.Services.Cards

- CardCreated
- CardRemoved
- CardRenamed
- BalanceIncreased
- BalanceDecreased
- CardBlocked
- CardActivated
- CardExpired

## Примитивы

#### CardState

- [ ] None = 0
- [x] Active = 1
- [ ] Blocked = 2
- [ ] Expired = 3

#### CountryCode

- [ ] None = 0
- [x] Russia = 7

#### UserRole

- [ ] None = 0
- [ ] Admin = 1
- [x] Customer = 2

#### UserState

- [ ] None = 0
- [x] NotActive = 1
- [ ] Active = 2
- [ ] Blocked = 3

## gRPC вызовы сервиса

#### ProfileGrpc

Инжект клиента и каналов нотификации с помощью DI контейнера

    private readonly ProfileGrpc.ProfileGrpcClient _client;
    private readonly INotificationsChannel _notificationsChannel;
    private readonly INotificationContext _notificationContext;
    ...
    public ProfileService(
       ProfileGrpc.ProfileGrpcClient client,
       INotificationsChannel notificationsChannel, 
       INotificationContext notificationContext)
    {
       _client = client;
       _notificationsChannel = notificationsChannel;
       _notificationContext = notificationContext;
    }

Смена ФИО пользователя

    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
       var result = await _client.ChangeNameAsync(request);
       await notification.Wait(_timeout, result.Version);
       if (!result.Version.HasValue)
           return;
    }

Смена телефона  

    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
       var result = await _client.ChangePhoneAsync(request);
       await notification.Wait(_timeout, result.Version);
       if (!result.Version.HasValue)
           return;
     }

Смена email 

    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
       var result = await _client.ChangeEmailAsync(request);
       await notification.Wait(_timeout, result.Version);
       if (!result.Version.HasValue)
           return;
    }

Смена фото 

    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
       var result = await _client.ChangePhotoAsync(request);
       await notification.Wait(_timeout, result.Version);
       if (!result.Version.HasValue)
           return;
    }
     
 #### UserGrpc
 
 Инжект клиента и каналов нотификации с помощью DI контейнера
 
    private readonly UserGrpc.UserGrpcClient _client;
    private readonly INotificationsChannel _notificationsChannel;
    private readonly INotificationContext _notificationContext;
    ...
    public IdentityService(
       UserGrpc.UserGrpcClient client,
       INotificationsChannel notificationsChannel, 
       INotificationContext notificationContext)
    {
       _client = client;
    }
   
 Создание пользователя
 
    await _userClient.CreateAsync(srvRequestCreate);
    
 Удаление пользователя
 
    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
        var srvResult = await _userClient.RemoveAsync(srvRequest);
        if (srvResult.Version.HasValue)
            await notification.Wait(_timeout, srvResult.Version);
    }
    
Смена роли
 
    using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
    {
        var srvResult = await _userClient.ChangeRoleAsync(srvRequest);
        if (srvResult.Version.HasValue)
            await notification.Wait(_timeout, srvResult.Version);
    }
    
Смена имени пользователя
    
     using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
     {
         var srvResult = await _userClient.ChangeUsernameAsync(srvRequest);
         if (srvResult.Version.HasValue)
             await notification.Wait(_timeout, srvResult.Version);
     }
     
Активация пользователя

     using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
     {
         var srvResult = await _userClient.ActivateAsync(srvRequest);
         if (srvResult.Version.HasValue)
             await notification.Wait(_timeout, srvResult.Version);
     }
     
Блокировка пользователя

     using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
     {
         var srvResult = await _userClient.BlockAsync(srvRequest);
         if (srvResult.Version.HasValue)
             await notification.Wait(_timeout, srvResult.Version);
     }
    
## Copyright
    
License [GNU GPLv3](http://www.gnu.org/licenses/gpl-3.0.txt)

© [Amethyst Pro](http://www.amethyst.pro) LLC, 2019