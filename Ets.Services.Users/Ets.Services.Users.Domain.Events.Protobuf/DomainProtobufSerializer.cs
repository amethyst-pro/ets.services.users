using Amethyst.Grpc.Protobuf;
using Amethyst.Streams.Domain;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Domain.Events.Protobuf
{
    public static class DomainProtobufSerializer
    {
        public static IDomainEventSerializer Instance { get; }

        static DomainProtobufSerializer()
        {
            var configuration = new SerializerConfiguration()

                .CreateMap<Email, EmailProto>()
                .CreateMap<Name, NameProto>()
                .CreateMap<PhoneNumber, PhoneNumberProto>()
                .CreateMap<Photo, PhotoProto>()
                .CreateMap<Username, UsernameProto>()

                .CreateMap<CardRemoved, CardRemovedProto>()
                .CreateMap<CardBlocked, CardBlockedProto>()
                .CreateMap<CardActivated, CardActivatedProto>()
                .CreateMap<CardUpdated, CardUpdatedProto>()
                .CreateMap<EmailChanged, EmailChangedProto>()
                .CreateMap<NameChanged, NameChangedProto>()
                .CreateMap<PhoneChanged, PhoneChangedProto>()
                .CreateMap<PhotoChanged, PhotoChangedProto>()
                .CreateMap<RoleChanged, RoleChangedProto>()
                .CreateMap<UserActivated, UserActivatedProto>()
                .CreateMap<UserBlocked, UserBlockedProto>()
                .CreateMap<UserCreated, UserCreatedProto>()
                .CreateMap<UserRemoved, UserRemovedProto>()
                .CreateMap<UsernameChanged, UsernameChangedProto>()
                
                .CreateMap<UserChanged, UserChangedProto>()

                .RegisterGuidId<UserId>()
                .RegisterGuidId<CardId>();
            
            Instance = configuration.CreateSerializer();
        }
    }
}