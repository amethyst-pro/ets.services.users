using DomainUserId = Ets.Services.Users.Domain.Events.ValueObjects.UserId;
using CardUserId = Ets.Services.Cards.Domain.Events.ValueTypes.UserId;

namespace Ets.Services.Users.Application.Converters
{
    public static class UserIdConverter
    {
        public static DomainUserId ToDomain(this CardUserId cardUser)
            => new DomainUserId(cardUser.Value);
    }
}