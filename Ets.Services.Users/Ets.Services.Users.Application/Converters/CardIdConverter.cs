using DomainCardId = Ets.Services.Users.Domain.Events.ValueObjects.CardId;
using CardCardId = Ets.Services.Cards.Domain.Events.ValueTypes.CardId;

namespace Ets.Services.Users.Application.Converters
{
    public static class CardIdConverter
    {
        public static DomainCardId ToDomain(this CardCardId cardUser)
            => new DomainCardId(cardUser.Value);
    }
}