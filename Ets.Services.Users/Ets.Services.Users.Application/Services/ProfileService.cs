using System.Threading.Tasks;
using Ets.Services.Users.Application.Commands;
using Ets.Services.Users.Application.Contracts.Repositories;
using Ets.Services.Users.Application.Contracts.Services;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Application.Services
{
    public sealed class ProfileService : IProfileService
    {
        private readonly IUserRepository _repository;
        private readonly ILogger<ProfileService> _logger;

        public ProfileService(
            IUserRepository repository,
            ILogger<ProfileService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<long?> ChangeNameAsync(ChangeName command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangeName(command.Name);
            
            if (!user.HasChanges)
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Name changed ({command.UserId}).");

            return user.CurrentVersion;
        }

        public async Task<long?> ChangeEmailAsync(ChangeEmail command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangeEmail(command.Email);
            
            if (!user.HasChanges)
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Email changed ({command.UserId}).");
            
            return user.CurrentVersion;
        }

        public async Task<long?> ChangePhoneAsync(ChangePhone command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangePhone(command.Phone);
            
            if (!user.HasChanges)
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Phone number changed ({command.UserId}).");
            
            return user.CurrentVersion;
        }

        public async Task<long?> ChangePhotoAsync(ChangePhoto command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangePhoto(command.Photo);
            
            if (!user.HasChanges)
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Photo changed ({command.UserId}).");
            
            return user.CurrentVersion;
        }
    }
}