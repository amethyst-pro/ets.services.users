using System.Linq;
using System.Threading.Tasks;
using Amethyst.Domain;
using Ets.Services.Users.Application.Commands;
using Ets.Services.Users.Application.Contracts.Services;
using Ets.Services.Users.Domain.Aggregates;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Application.Services
{
    public class LifetimeService : ILifetimeService
    {
        private readonly IRepository<User, UserId> _repository;
        private readonly ILogger<LifetimeService> _logger;
        private readonly IChangesService _changesService;

        public LifetimeService(
            IRepository<User, UserId> repository,
            ILogger<LifetimeService> logger, 
            IChangesService changesService)
        {
            _repository = repository;
            _logger = logger;
            _changesService = changesService;
        }
        
        public async Task<long?> CreateUserAsync(CreateUser command)
        {
            var user = (await _repository.GetAsync(command.UserId)).SingleOrDefault();
            if (user != null)
                return default;
            
            var newUser = new User(
                command.UserId, 
                command.Username, 
                command.Email, 
                command.Role,
                _changesService);
            
            await _repository.SaveAsync(newUser); 
            
            _logger.LogDebug($"User created ({command.UserId}).");

            return newUser.Version.OrDefault();
        }

        public async Task<long?> RemoveAsync(RemoveUser command)
        {
            var user = (await _repository.GetAsync(command.UserId)).SingleOrDefault();
            if (user == null || user.IsRemoved)
                return default;
            
            user.Remove();

            if (!user.HasChanges())
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"User removed ({command.UserId}).");

            return user.Version.OrDefault();
        }
    }
}