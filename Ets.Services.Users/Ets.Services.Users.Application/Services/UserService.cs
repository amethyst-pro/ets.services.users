using System.Threading.Tasks;
using Ets.Services.Users.Application.Commands;
using Ets.Services.Users.Application.Contracts.Repositories;
using Ets.Services.Users.Application.Contracts.Services;
using Microsoft.Extensions.Logging;
using SharpJuice.Essentials;

namespace Ets.Services.Users.Application.Services
{
    public sealed class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly ILogger<UserService> _logger;
        private readonly IClock _clock;
        private readonly IExternalEventsService _eventsService;

        public UserService(
            IUserRepository repository,
            ILogger<UserService> logger,
            IClock clock,
            IExternalEventsService eventsService)
        {
            _repository = repository;
            _logger = logger;
            _clock = clock;
            _eventsService = eventsService;
        }

        public async Task<long?> ChangeUsernameAsync(ChangeUsername command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangeUsername(command.Username);
            
            if (!user.HasChanges)
                return default;

            await _eventsService.SendAsync(user.GetChanges(_clock.Now));
            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Username changed ({command.UserId}).");
            
            return user.CurrentVersion;
        }

        public async Task<long?> ActivateAsync(ActivateUser command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.Activate();
            
            if (!user.HasChanges)
                return default;

            await _eventsService.SendAsync(user.GetChanges(_clock.Now));
            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"User activated ({command.UserId}).");
            
            return user.CurrentVersion;
        }

        public async Task<long?> BlockAsync(BlockUser command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.Block();
            
            if (!user.HasChanges)
                return default;

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"User blocked ({command.UserId}).");
            
            return user.CurrentVersion;
        }

        public async Task<long?> ChangeRoleAsync(ChangeRole command)
        {
            var user = await _repository.GetOrCreate(command.UserId);
            if (user.IsRemoved)
                return default;
            
            user.ChangeRole(command.Role);
            
            if (!user.HasChanges)
                return default;

            await _eventsService.SendAsync(user.GetChanges(_clock.Now));
            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"User role changed ({command.UserId}).");
            
            return user.CurrentVersion;
        }
    }
}