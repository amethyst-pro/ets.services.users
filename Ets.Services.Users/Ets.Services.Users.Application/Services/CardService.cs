using System.Threading.Tasks;
using Amethyst.Domain.ValueObjects;
using Amethyst.Subscription.Abstractions;
using Ets.Services.Cards.Domain.Events;
using Ets.Services.Users.Application.Contracts.Repositories;
using Ets.Services.Users.Application.Converters;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Application.Services
{
    public sealed class CardService 
        : IEventHandler<CardCreated>, 
            IEventHandler<CardRemoved>,
            IEventHandler<BalanceIncreased>,
            IEventHandler<BalanceDecreased>,
            IEventHandler<CardBlocked>,
            IEventHandler<CardActivated>,
            IEventHandler<CardExpired>
    {
        private readonly IUserRepository _repository;
        private readonly ILogger<ProfileService> _logger;

        public CardService(
            IUserRepository repository,
            ILogger<ProfileService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public Task HandleAsync(CardCreated @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();

            return AddOrUpdateCardAsync(userId, cardId, default);
        }

        public Task HandleAsync(BalanceIncreased @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();

            return AddOrUpdateCardAsync(userId, cardId, @event.Balance);
        }

        public Task HandleAsync(BalanceDecreased @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();

            return AddOrUpdateCardAsync(userId, cardId, @event.Balance);
        }

        public async Task HandleAsync(CardActivated @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();
            
            var user = await _repository.GetOrCreate(userId);
            if (user.IsRemoved)
                return;
            
            user.ActivateCard(cardId);
            
            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Card {cardId} activated ({userId}).");
        }
        
        public async Task HandleAsync(CardBlocked @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();
            
            var user = await _repository.GetOrCreate(userId);
            if (user.IsRemoved)
                return;
            
            user.BlockCard(cardId);
            
            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Card {cardId} blocked ({userId}).");
        }
        
        private async Task AddOrUpdateCardAsync(UserId userId, CardId cardId, Money balance)
        {
            var user = await _repository.GetOrCreate(userId);
            if (user.IsRemoved)
                return;
            
            user.AddOrUpdateCard(cardId, balance);

            await _repository.SaveAsync(user);
            
            _logger.LogDebug($"Card {cardId} updated ({userId}).");
        }
        
        public async Task HandleAsync(CardRemoved @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();

            await RemoveAsync(userId, cardId);
            
            _logger.LogDebug($"Card {cardId} removed ({userId}).");
        }

        public async Task HandleAsync(CardExpired @event)
        {
            var userId = @event.UserId.ToDomain();
            var cardId = @event.CardId.ToDomain();

            await RemoveAsync(userId, cardId);
            
            _logger.LogDebug($"Card {cardId} expired ({userId}).");
        }

        private async Task RemoveAsync(UserId userId, CardId cardId)
        {
            var user = await _repository.GetOrCreate(userId);
            if (user.IsRemoved)
                return;
            
            user.RemoveCard(cardId);

            await _repository.SaveAsync(user);
        }
    }
}