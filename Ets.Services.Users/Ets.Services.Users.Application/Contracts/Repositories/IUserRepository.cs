using System.Threading.Tasks;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Contracts.Repositories
{
    public interface IUserRepository
    {
        Task<IUser> GetOrCreate(UserId userId);

        Task SaveAsync(IUser user);
    }
}