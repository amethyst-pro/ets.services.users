using System.Threading.Tasks;
using Ets.Services.Users.Application.Commands;

namespace Ets.Services.Users.Application.Contracts.Services
{
    public interface IProfileService
    {
        Task<long?> ChangeNameAsync(ChangeName command);
        
        Task<long?> ChangeEmailAsync(ChangeEmail command);

        Task<long?> ChangePhoneAsync(ChangePhone command);

        Task<long?> ChangePhotoAsync(ChangePhoto command);
    }
}