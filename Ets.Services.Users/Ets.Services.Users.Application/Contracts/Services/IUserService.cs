using System.Threading.Tasks;
using Ets.Services.Users.Application.Commands;

namespace Ets.Services.Users.Application.Contracts.Services
{
    public interface IUserService
    {
        Task<long?> ChangeUsernameAsync(ChangeUsername command);

        Task<long?> ActivateAsync(ActivateUser command);

        Task<long?> BlockAsync(BlockUser command);

        Task<long?> ChangeRoleAsync(ChangeRole command);
    }
}