using System.Threading.Tasks;
using Ets.Services.Users.Domain.Events;

namespace Ets.Services.Users.Application.Contracts.Services
{
    public interface IExternalEventsService
    {
        Task SendAsync(UserChanged changed);
    }
}