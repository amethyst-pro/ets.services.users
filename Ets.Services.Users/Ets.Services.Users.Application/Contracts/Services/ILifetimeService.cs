using System.Threading.Tasks;
using Ets.Services.Users.Application.Commands;

namespace Ets.Services.Users.Application.Contracts.Services
{
    public interface ILifetimeService
    {
        Task<long?> CreateUserAsync(CreateUser command);

        Task<long?> RemoveAsync(RemoveUser command);
    }
}