using System.Threading.Tasks;
using Amethyst.Domain;
using Ets.Services.Users.Application.Contracts.Repositories;
using Ets.Services.Users.Domain.Aggregates;
using Ets.Services.Users.Domain.Contracts;
using Ets.Services.Users.Domain.Events.ValueObjects;
using Microsoft.Extensions.Logging;

namespace Ets.Services.Users.Application.Ports.Repositories
{
    public sealed class UserRepository : IUserRepository
    {
        private readonly IRepository<User, UserId> _repository;
        private readonly ILogger<UserRepository> _logger;

        public UserRepository(
            IRepository<User, UserId> repository, 
            ILogger<UserRepository> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<IUser> GetOrCreate(UserId userId)
        {
            var user = await _repository.GetAsync(userId);
            if (user.Any())
                return user.Single();
            
            _logger.LogDebug($"Get removed user {userId}");
            
            return new NullUser();
        }

        public async Task SaveAsync(IUser user)
        {
            if (user is User aggregate)
                await _repository.SaveAsync(aggregate);
        }
    }
}