using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangeEmail
    {
        public ChangeEmail(UserId userId, Email email)
        {
            UserId = userId;
            Email = email;
        }

        public UserId UserId { get; } 
        
        public Email Email { get; }
    }
}