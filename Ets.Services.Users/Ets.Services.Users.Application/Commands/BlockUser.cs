using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct BlockUser
    {
        public BlockUser(UserId userId)
            => UserId = userId;

        public UserId UserId { get; }
    }
}