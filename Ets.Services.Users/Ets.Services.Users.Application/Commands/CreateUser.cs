using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct CreateUser
    {
        public CreateUser(
            UserId userId, 
            Username username, 
            Email email, 
            UserRole role)
        {
            UserId = userId;
            Username = username;
            Email = email;
            Role = role;
        }

        public UserId UserId { get; }
            
        public Username Username { get; }
        
        public Email Email { get; }
        
        public UserRole Role { get; }
    }
}