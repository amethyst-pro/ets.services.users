using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangePhone
    {
        public ChangePhone(UserId userId, PhoneNumber phone)
        {
            UserId = userId;
            Phone = phone;
        }

        public UserId UserId { get; } 
        
        public PhoneNumber Phone { get; }
    }
}