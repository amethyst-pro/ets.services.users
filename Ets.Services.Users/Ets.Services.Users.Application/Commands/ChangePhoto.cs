using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangePhoto
    {
        public ChangePhoto(UserId userId, Photo photo)
        {
            UserId = userId;
            Photo = photo;
        }

        public UserId UserId { get; }
        
        public Photo Photo { get; }
    }
}