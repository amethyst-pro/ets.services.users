using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangeName
    {
        public ChangeName(UserId userId, Name name)
        {
            UserId = userId;
            Name = name;
        }

        public UserId UserId { get; }
        
        public Name Name { get; }
    }
}