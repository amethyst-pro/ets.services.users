using Ets.Services.Users.Domain.Events.Primitives;
using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangeRole
    {
        public ChangeRole(UserId userId, UserRole role)
        {
            UserId = userId;
            Role = role;
        }

        public UserId UserId { get; }

        public UserRole Role { get; }
    }
}