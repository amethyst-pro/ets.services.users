using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ChangeUsername
    {
        public ChangeUsername(UserId userId, Username username)
        {
            UserId = userId;
            Username = username;
        }

        public UserId UserId { get; }
        
        public Username Username { get; }
    }
}