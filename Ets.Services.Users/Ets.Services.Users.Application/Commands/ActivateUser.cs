using Ets.Services.Users.Domain.Events.ValueObjects;

namespace Ets.Services.Users.Application.Commands
{
    public readonly struct ActivateUser
    {
        public ActivateUser(UserId userId)
            => UserId = userId;

        public UserId UserId { get; }
    }
}