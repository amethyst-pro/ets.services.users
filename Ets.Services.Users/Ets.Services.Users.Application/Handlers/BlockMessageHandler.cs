using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging;
using Polly;

namespace Ets.Services.Users.Application.Handlers
{
    public sealed class BlockMessageHandler
    {
        private readonly ILoggerFactory _loggerFactory;
        private const int Attempts = 5;
        private const int MaxDegree = 10;

        public BlockMessageHandler(ILoggerFactory loggerFactory)
            =>  _loggerFactory = loggerFactory;

        public Task ExecuteAsync<T>(IEnumerable<T> messages, Func<T, Task> func)
        {
            var actionBlock = Create(func);

            foreach (var tuple in messages)
                actionBlock.Post(tuple);

            actionBlock.Complete();
            return actionBlock.Completion;
        }
        
        private ActionBlock<T> Create<T>(Func<T,Task> action)
        {
            var logger = _loggerFactory.CreateLogger<T>();
            var policy = Policy.Handle<Exception>()
                .WaitAndRetryAsync(
                    Attempts,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (ex, timeout, attempt, __) => logger.LogError(ex,
                        $"Item processing failed. Msg='{typeof(T).Name}' Attempt = {attempt}, timeout = {timeout}"));

            return new ActionBlock<T>(
                m => policy.ExecuteAsync(() => action(m)),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = MaxDegree });
        }
    }
}